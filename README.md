This is very basic and simple HTML, CSS and JavaScript Tutorial Session.

Hyper Text Markup Language - HTML

- it is the structural component of the web Page. like pillars and walls of the building.

Cascade Styling Sheet - CSS

- it is designing or styling component of the web Page like paint positioning and styling all.

JavaScript - JS

- it is the functioning component of the web Page. We can use it to perform many dynamic actions based on the circumstances or parameters or user inputs to make the web page look more functional and alive. It also reduce pressure on the server as it is able to do some sorts of validations and checks in the user ends.

prerequirements

- students are expected to know basics of computer
- understanding about files and what is web page

Requirements:

- PC or Laptop
- (optional) internet connection for research and detailed information on the contents

Some Notes about the contents:

- in the root folder there are some html files which were used to demonstrate examples while teaching
- therefore, rather than looking at the web page itself, students are requested to look at the code so as to understand how it works or how its written.
- form.html has javascript feature used for the validation purpose.
- another.html deals with media concepts
- layout.html has the concepts about layout
- There is an example of an attractive web page design with the help of bootstart and jquery(can be avoided) inside the bootstrap-example folder. It also shows students how to use external libraries and use them as per our requirements.

Students are requested to contribute to this repo so as to share their ideas and/or queries but in different branch. Also try to provide as many comments on your code so that new students can understand easily.
